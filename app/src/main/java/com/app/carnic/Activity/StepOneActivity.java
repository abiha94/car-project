package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.app.carnic.Dialog.EslahPelakDialog;
import com.app.carnic.R;
import com.app.carnic.Util.CarUtil;

public class StepOneActivity extends AppCompatActivity {

    View age,ths,jns;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_one);
        age = findViewById(R.id.st1_age);
        jns = findViewById(R.id.st1_jns);
        ths = findViewById(R.id.st1_ths);
        imageView = findViewById(R.id.imageView11);
        CarUtil.loadImageFromDrawble(this,R.drawable.stepbg,imageView);

        age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepOneActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        int i = item.getItemId();
                        if (i == 1) {

                            return true;
                        } else if (i == 2) {
                            //handle comment
                            return true;
                        } else {

                            return false;
                        }
                    }

                });
            }
        });
        ths.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepOneActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        int i = item.getItemId();
                        if (i == 1) {

                            return true;
                        } else if (i == 2) {
                            //handle comment
                            return true;
                        } else {

                            return false;
                        }
                    }

                });
            }
        });
        jns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepOneActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        int i = item.getItemId();
                        if (i == 1) {

                            return true;
                        } else if (i == 2) {
                            //handle comment
                            return true;
                        } else {

                            return false;
                        }
                    }

                });
            }
        });
        findViewById(R.id.stepnxt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                   startActivity(new Intent(getApplicationContext(),StepTowActivity.class));
            }
        });
         findViewById(R.id.stepprvs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                   finish();
            }
        });
    }
}