package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.Toast;

import com.app.carnic.DataSingelton;
import com.app.carnic.Helper.UserPrefHelper;
import com.app.carnic.MyApp;
import com.app.carnic.R;

public class SplashScreenActivity extends AppCompatActivity {

    UserPrefHelper userPrefHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        userPrefHelper = new UserPrefHelper(this);
        Handler handler = new Handler();;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               if (userPrefHelper.getUserisLogin()){
                   startActivity(new Intent(getApplicationContext(),SelectItemActivity.class));
               }else{
                   startActivity(new Intent(getApplicationContext(),LoginActivity.class));
               }
            }
        }, 1000);
    }
}