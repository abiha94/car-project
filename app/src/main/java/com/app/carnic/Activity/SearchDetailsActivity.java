package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.app.carnic.Dialog.EslahDialog;
import com.app.carnic.Dialog.EslahPelakDialog;
import com.app.carnic.R;
import com.app.carnic.Util.CarUtil;

public class SearchDetailsActivity extends AppCompatActivity {

    ImageView one,tow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_details);
        one = findViewById(R.id.imageView4);
        tow = findViewById(R.id.imageView8);
        CarUtil.loadImageFromDrawble(this,R.drawable.serachformbg,one);
        CarUtil.loadImageFromDrawble(this,R.drawable.searchcar,tow);

        findViewById(R.id.relativeLayosut6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(SearchDetailsActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "اصلاح پلاک");
                menu.getMenu().add(Menu.NONE, 2, 2, "اصلاح مالک");
                menu.getMenu().add(Menu.NONE, 3, 3, "انتخاب");
                menu.show();

                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        int i = item.getItemId();
                        if (i == 1) {
                            new EslahDialog().show(getSupportFragmentManager(),"");

                            return true;
                        } else if (i == 2) {
                            new EslahPelakDialog().show(getSupportFragmentManager(),"");
                            return true;
                        } else {
                            startActivity(new Intent(getApplicationContext(),StepOneActivity.class));
                            return false;
                        }
                    }

                });
            }
        });
    }

}