package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.carnic.Config;
import com.app.carnic.Dialog.LoadingDialog;
import com.app.carnic.Helper.ApiHelper;
import com.app.carnic.Helper.UserPrefHelper;
import com.app.carnic.Interface.RequestInterface;
import com.app.carnic.R;
import com.app.carnic.Util.JsonUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements RequestInterface {

    EditText username,password;
    View btn;
    ApiHelper apiHelper;
    UserPrefHelper helper ;
    LoadingDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckEditText();
            }
        });
    }

    private void CheckEditText() {
        if (username.getText().toString().trim().length()<3){
            toastMessege("نام کاربری وارد نشده است");
        }else if (password.getText().toString().trim().length()<3){
            toastMessege("رمز عبور وارد نشده است");
        }else {
            sendLoginRequest(username.getText().toString(),password.getText().toString());
        }
    }

    private void sendLoginRequest(String username, String password) {
      /*  JSONObject jsonObject = new JSONObject();
        try {
            Map<String, Object> map = new HashMap<>();

            map.put("PhoneNumber", username);
            map.put("Password", password);
            map.put("ReturnUrl", "");
            apiHelper.postRequest(Config.APPSERVERADDRES+"applogin/login",new JSONObject(map),1);

        } catch (Exception e) {
            e.printStackTrace();
        }*/
        try {

            helper.setUserLogin(true);
        }catch (Exception e){
            Toast.makeText(this, e.getLocalizedMessage()+"error json", Toast.LENGTH_SHORT).show();
        }finally {
            startActivity(new Intent(getApplicationContext(),SelectItemActivity.class));
            finish();
        }
    }

    private void toastMessege(String s) {
        Toast.makeText(this, s+"", Toast.LENGTH_SHORT).show();
    }

    private void initView() {
        dialog= new LoadingDialog();
        username = findViewById(R.id.login_usrname);
        password = findViewById(R.id.login_password);
        btn = findViewById(R.id.login_btn);
        apiHelper = new ApiHelper(this,this);
        helper = new UserPrefHelper(this);
    }

    @Override
    public void RequestRecive(String body, int code) {

        try {
            String token = JsonUtil.getTokenFromJson(body);

            helper.setUserLogin(true);
            helper.saveToken(token);
        }catch (Exception e){
            Toast.makeText(this, e.getLocalizedMessage()+"error json", Toast.LENGTH_SHORT).show();
        }finally {
            startActivity(new Intent(getApplicationContext(),SearchCarActivity.class));
            finish();
        }

    }

    @Override
    public void startRequest(int code) {

        dialog.show(getSupportFragmentManager(),"");
    }

    @Override
    public void ErrorRequest(String e) {
        dialog.dismiss();
        if (e.equals(400)){
            toastMessege("نام کاربری و رمز عبور شما صحیح نیست");
        }
    }
}