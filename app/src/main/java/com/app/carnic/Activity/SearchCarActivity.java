package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.carnic.DataSingelton;
import com.app.carnic.Interface.RequestInterface;
import com.app.carnic.MyApp;
import com.app.carnic.R;
import com.app.carnic.Util.CarUtil;
import com.bumptech.glide.Glide;

public class SearchCarActivity extends AppCompatActivity implements RequestInterface {


    ImageView one,tow;
    EditText shasiCode,natCode,trackCode;
    View btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_car);
        DataSingelton dataSingeltonb = MyApp.getContext();
        initView();




        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shasiCode.getText().toString().trim().length() >= 3){
                    sendApiRequest(shasiCode.getText().toString(),"","");
                }else  if (natCode.getText().toString().trim().length() >= 3){
                    sendApiRequest("",natCode.getText().toString(),"");
                }
                else  if (trackCode.getText().toString().trim().length() >= 3){
                    sendApiRequest("","",trackCode.getText().toString());
                }else {
                    Toast.makeText(SearchCarActivity.this, "شما حداقل باید یک مورد را پر کنید", Toast.LENGTH_SHORT).show();
                }

            }
        });

        CarUtil.loadImageFromDrawble(this,R.drawable.serachformbg,one);
        CarUtil.loadImageFromDrawble(this,R.drawable.searchcar,tow);

    }

    private void sendApiRequest(String shasiCode,String natCode,String trackCode) {
        startActivity(new Intent(getApplicationContext(),SearchDetailsActivity.class));

    }

    private void initView() {
        shasiCode = findViewById(R.id.sch_shasi);
        natCode = findViewById(R.id.sch_code);
        trackCode = findViewById(R.id.sch_nobat);
        one = findViewById(R.id.imageView4);
        tow = findViewById(R.id.imageView8);
        btn = findViewById(R.id.cardView);
    }

    @Override
    public void RequestRecive(String body, int code) {

    }

    @Override
    public void startRequest(int code) {

    }

    @Override
    public void ErrorRequest(String errorscode) {

    }
}