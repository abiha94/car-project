package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.app.carnic.R;

public class SelectItemActivity extends AppCompatActivity {

    View viw1;
    View viw2;
    View viw3;
    View viw4;
    View viw5;
    View viw6;
    View viw7;
    View viw8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_item);
        viw1 = findViewById(R.id.relativeLayout3);
        viw2 = findViewById(R.id.relativeLayout31);
        viw3 = findViewById(R.id.relativeLayout32);
        viw4 = findViewById(R.id.relativeLayout4);
        viw5 = findViewById(R.id.relativeLayout41);
        viw6 = findViewById(R.id.relativeLayout42);
        viw7 = findViewById(R.id.relativeLayout5);
        viw8 = findViewById(R.id.relativeLayout51);
    }

    public void itemClick(View view) {
        switch (view.getId()){
            case R.id.relativeLayout3:
                startActivity(new Intent(getApplicationContext(),SearchCarActivity.class));
                break;
            default:
                Toast.makeText(this, "شما دسترسی به این قسمت را ندارید", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}