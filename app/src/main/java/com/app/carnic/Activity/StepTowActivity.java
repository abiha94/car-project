package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.carnic.Adapter.EzharatlistAdapter;
import com.app.carnic.Model.EzharatModel;
import com.app.carnic.R;
import com.app.carnic.Util.CarUtil;

import java.util.ArrayList;
import java.util.List;

public class StepTowActivity extends AppCompatActivity {

    View recStart,recStop,submt,delete;
    Chronometer chronometer;
    EditText ezMoshtari;
    EditText ezCarshensh;
    TextView ezharat;
    String ezhara;
    ImageView imageView;
    EzharatlistAdapter ezharatlistAdapter;
    List<EzharatModel> ezharatModelList = new ArrayList<>();
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_tow);
        recStart = findViewById(R.id.st2_rcstart);
        recStop = findViewById(R.id.st2_rcstop);
        chronometer = findViewById(R.id.st2_ch);
        ezCarshensh = findViewById(R.id.st2_ezkrshns);
        submt= findViewById(R.id.st2_submit);
        delete= findViewById(R.id.st2_rcstop2);
        ezMoshtari = findViewById(R.id.st2_ezmosh);
        imageView= findViewById(R.id.imageView11);
        recyclerView= findViewById(R.id.recyclerview);
        CarUtil.loadImageFromDrawble(this,R.drawable.stepbg,imageView);
        delete.setVisibility(View.INVISIBLE);

       // chronometer.stop();
        ezhara = "";
        recStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.setBase(SystemClock.elapsedRealtime());

                chronometer.start();
            }
        });
        recStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete.setVisibility(View.VISIBLE);

                chronometer.stop();
            }
        });
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                delete.setVisibility(View.INVISIBLE);

            }
        });
        submt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String SezMoshtari = ezMoshtari.getText().toString();
                String SezCarshensh = ezCarshensh.getText().toString();
              if (
                      SezMoshtari.length() >2 &&
                              SezCarshensh.length() >2
              ){

                  ezharatModelList.add(new EzharatModel(SezCarshensh,SezMoshtari));
                  setRecyclerView(ezharatModelList);
               /*   ezhara += ezMoshtari.getText().toString()+" - " +ezCarshensh.getText().toString() +"\n";
                  ezharat.setText(ezhara);*/
                  ezMoshtari.setText("");
                  ezCarshensh.setText("");
              }
            }
        });
        findViewById(R.id.stepnxt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(),StepThreeActivity.class));
            }
        });
        findViewById(R.id.stepprvs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.stop();
            }
        });
    }

    private void setRecyclerView(List<EzharatModel> ezharatModelLists) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        ezharatlistAdapter = new EzharatlistAdapter(ezharatModelLists,this);
        recyclerView.setAdapter(ezharatlistAdapter);
    }
}