package com.app.carnic.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.carnic.Adapter.PhotolistAdapter;
import com.app.carnic.DataSingelton;
import com.app.carnic.Model.PhotolistModel;
import com.app.carnic.Model.Step3Model;
import com.app.carnic.MyApp;
import com.app.carnic.R;
import com.app.carnic.Util.CarUtil;
import com.app.carnic.Util.DrawingClass;
import com.github.dhaval2404.imagepicker.ImagePicker;

import java.util.ArrayList;
import java.util.List;

import tech.picnic.fingerpaintview.FingerPaintImageView;

public class StepThreeActivity extends AppCompatActivity {
    ImageView imageView;
    ImageView imageView2;

    List<PhotolistModel> models = new ArrayList<>();
    PhotolistAdapter adapter;
    RecyclerView recyclerView;
    FingerPaintImageView fingerPaintImageView;
    DataSingelton dataSingeltonb;
    Step3Model step3Model;
    boolean isFirstPic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DrawingClass mDrawingView=new DrawingClass(this);
        setContentView(R.layout.activity_step_three);
        dataSingeltonb = MyApp.getContext();
        step3Model = dataSingeltonb.getStep3Model();

        isFirstPic = true;
        imageView= findViewById(R.id.imageView11);
        CarUtil.loadImageFromDrawble(this,R.drawable.stepbg,imageView);
        imageView2= findViewById(R.id.imageView9);
        recyclerView= findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        fingerPaintImageView= findViewById(R.id.finger);

        findViewById(R.id.stepnxt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(getApplicationContext(),StepFourActivity.class));
            }
        });
        findViewById(R.id.stepprvs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
           findViewById(R.id.reltie).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImagePicker.Companion
                                .with(StepThreeActivity.this)
                                .compress(10000)
                                .crop()
                                .start();

                    }
                });

    }
    public static Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){


            if (isFirstPic){
                models.add(new PhotolistModel(data.getData(),data.getData().toString()));
                Toast.makeText(this, "size1="+models.size(), Toast.LENGTH_SHORT).show();
                step3Model.setModels(models);
                dataSingeltonb.setStep3Model(step3Model);

            }else {
                List<PhotolistModel> oldmodels =  dataSingeltonb.getStep3Model().getModels();

                oldmodels.add(new PhotolistModel(data.getData(),data.getData().toString()));
                step3Model.setModels(oldmodels);
                Toast.makeText(this, "size2="+models.size(), Toast.LENGTH_SHORT).show();

                dataSingeltonb.setStep3Model(step3Model);

            }
            isFirstPic = false;

            adapter = new PhotolistAdapter(models,this);
            recyclerView.setAdapter(adapter);

        }else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, "فایل انتخابی شما درست نیست", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "شما تصویری برای بارگزاری انتخاب نکردید", Toast.LENGTH_SHORT).show();
        }
    }
}