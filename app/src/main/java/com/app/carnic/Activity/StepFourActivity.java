package com.app.carnic.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.app.carnic.R;
import com.app.carnic.Util.CarUtil;

public class StepFourActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_four);
        ImageView imageView= findViewById(R.id.imageView11);
        CarUtil.loadImageFromDrawble(this,R.drawable.stepbg,imageView);


        findViewById(R.id.st4_st1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepFourActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();


            }
        });

          findViewById(R.id.st4_st2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepFourActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();


            }
        });

          findViewById(R.id.st4_st3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepFourActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();


            }
        });

          findViewById(R.id.st4_st4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepFourActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();


            }
        });

          findViewById(R.id.st4_st5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepFourActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();


            }
        });

          findViewById(R.id.st4_st6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(StepFourActivity.this, v);
                menu.getMenu().add(Menu.NONE, 1, 1, "تست");
                menu.getMenu().add(Menu.NONE, 2, 2, "تست");

                menu.show();


            }
        });



        findViewById(R.id.stepnxt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(),StepFiveActivity.class));
            }
        });
        findViewById(R.id.stepprvs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }
}