package com.app.carnic.Util;

import android.content.Context;
import android.widget.ImageView;

import com.app.carnic.R;
import com.bumptech.glide.Glide;

public class CarUtil {
    public static void loadImageFromDrawble(Context context, int res, ImageView imageView){
        Glide
                .with(context)
                .load(res)
                .centerCrop()
                .into(imageView);
    }
}
