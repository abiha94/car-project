package com.app.carnic;

import com.app.carnic.Model.Step3Model;

public class DataSingelton {
    String editValue;

    private static DataSingelton ourInstance = new DataSingelton();
    private static Step3Model step3Model = new Step3Model();
    public static DataSingelton getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataSingelton();
        }
        if (step3Model == null) {
            step3Model = new Step3Model();
        }
        return ourInstance;
    }
    private DataSingelton() { }
    public void setText(String editValue) {
        this.editValue = editValue;
    }
    public String getText() {
        return editValue;
    }

    public Step3Model getStep3Model() {
        return step3Model;
    }

    public void setStep3Model(Step3Model step3Model) {
        this.step3Model = step3Model;
    }
}