package com.app.carnic.Helper;

import android.content.Context;
import android.content.SharedPreferences;

public class UserPrefHelper {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    public UserPrefHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("userPref",Context.MODE_PRIVATE);
        this.context = context;
        editor = sharedPreferences.edit();
    }

    public void saveToken(String Token){
        editor.putString("token",Token);
        editor.apply();

    }
    public String getToken(){
        return sharedPreferences.getString("token",null);
    }

    public boolean getUserisLogin() {
        return sharedPreferences.getBoolean("userisLogin",false);

    }

    public void logOutUser(){
        setUserLogin(false);
        editor.remove("token").apply();
    }

    public void setUserLogin(boolean islogin){
        editor.putBoolean("userisLogin",islogin);
        editor.apply();
    }
}
