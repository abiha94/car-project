package com.app.carnic.Helper;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.app.carnic.Config;
import com.app.carnic.Interface.RequestInterface;

import org.json.JSONObject;

import okhttp3.Response;


public class ApiHelper {

    Context context;
    RequestInterface requestInterface;
    UserPrefHelper userPrefHelper;

    public ApiHelper(Context context, RequestInterface requestInterface) {
        this.context = context;
        this.requestInterface = requestInterface;
        AndroidNetworking.initialize(context);
        userPrefHelper = new UserPrefHelper(context);
    }

    public int getRequestMethod(String url, final int req){
        requestInterface.startRequest(req);
        String url2 =  "https://client.raman24.com/api/v1/auth/login";
        AndroidNetworking.get(url)
                .setPriority(Priority.MEDIUM)
                .addHeaders("access-token","YWRtaW5AZ21haWwuY29tTU1UUlI5Tk9nb2dxMFgxTFAyQm55NjZ0UU1mbXR6")

                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        requestInterface.RequestRecive(response,req);
                    }

                    @Override
                    public void onError(ANError anError) {
                        requestInterface.ErrorRequest(String.valueOf(anError.getErrorCode()));

                    }
                });


        return req;
    }
    public int getRequestMethodWithToken(String url, final int code,String token){
        requestInterface.startRequest(code);
        String url2 =  "https://client.raman24.com/api/v1/auth/login";
        AndroidNetworking.get(url)
                .setPriority(Priority.MEDIUM)
                .addHeaders("access-token","YWRtaW5AZ21haWwuY29tTU1UUlI5Tk9nb2dxMFgxTFAyQm55NjZ0UU1mbXR6")
                .addHeaders("Authorization","Bearer "+userPrefHelper.getToken())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        requestInterface.RequestRecive(response,code);
                        Log.d(Config.LOGTAG,"Response Code"+code+"= "+response);

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(Config.LOGTAG,"Error Request in Code ="+code + "\n Error Details:  " +anError.getErrorDetail());
                        requestInterface.ErrorRequest("1");

                    }
                });


        return code;
    }

    public int postRequest(String url, JSONObject object, final int req){
        requestInterface.startRequest(req);
        String url2 =  "https://client.raman24.com/api/v1/auth/login";
        Log.d(Config.LOGTAG,"Map = "+object.toString());
        Log.d(Config.LOGTAG,"Url = "+url);

        AndroidNetworking.post(url)
                .addJSONObjectBody(object) // posting json
                .setPriority(Priority.MEDIUM)
                .setContentType("application/json")
               // .addHeaders("access-token","YWRtaW5AZ21haWwuY29tTU1UUlI5Tk9nb2dxMFgxTFAyQm55NjZ0UU1mbXR6")
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(Config.LOGTAG,"body = "+response);

                        requestInterface.RequestRecive(response,req);
                    }

                    @Override
                    public void onError(ANError error) {

                        requestInterface.ErrorRequest(String.valueOf(error.getErrorCode()));

                    }
                });




        return req;
    }
    public int postRequestWithToken(String url, JSONObject object, final int req, String token){
        requestInterface.startRequest(req);

        String url2 =  "https://client.raman24.com/api/v1/auth/login";
        AndroidNetworking.post(url)
                .addJSONObjectBody(object) // posting json
                .setPriority(Priority.MEDIUM)
                .setContentType("application/json")

          //      .addHeaders("access-token","YWRtaW5AZ21haWwuY29tTU1UUlI5Tk9nb2dxMFgxTFAyQm55NjZ0UU1mbXR6")
            //    .addHeaders("Authorization","Bearer "+userPrefHelper.getToken())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {

                        requestInterface.RequestRecive(response,req);
                    }

                    @Override
                    public void onError(ANError error) {
                        if (error.getErrorCode() != 0) {
                            // received error from server
                            // error.getErrorCode() - the error code from server
                            // error.getErrorBody() - the error body from server
                            // error.getErrorDetail() - just an error detail
                            Log.d(Config.LOGTAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(Config.LOGTAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(Config.LOGTAG,"onError errorDetail : " + error.getErrorDetail());
                            // get parsed error object (If ApiError is your class)

                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(Config.LOGTAG, "onError errorDetail : " + error.getErrorDetail());
                        }

                    }
                });


        return req;
    }
}
