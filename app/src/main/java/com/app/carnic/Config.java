package com.app.carnic;

public class Config {
    public static final String APP_PREF_NAME = "raman";
    public static final String APPSERVERADDRES = "https://app.mvms.ir/api/";
    public static final String APPSERVERADDRES2 = "https://client.raman24.com/api/v2/";
    public static final String LOGTAG = "LOGAPP";
    public static final String DATABASE = "ramandatabase.db";
}
