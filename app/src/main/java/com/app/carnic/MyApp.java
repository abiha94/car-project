package com.app.carnic;

import android.app.Application;

public class MyApp extends Application {


    private static DataSingelton sContextReference;

    @Override
    public void onCreate() {
        super.onCreate();
        sContextReference =DataSingelton.getInstance();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
       ;
    }

    public static DataSingelton getContext() {
        return sContextReference;
    }

}

