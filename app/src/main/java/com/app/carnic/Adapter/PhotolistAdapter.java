package com.app.carnic.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.carnic.Model.PhotolistModel;
import com.app.carnic.R;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class PhotolistAdapter extends RecyclerView.Adapter<PhotolistAdapter.PhotolistViewHolder> {

    List<PhotolistModel> photolistModels = new ArrayList<>();
    Context context;

    public PhotolistAdapter(List<PhotolistModel> photolistModels, Context context) {
        this.photolistModels = photolistModels;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public PhotolistViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.recy_photolist,parent,false);

        return new PhotolistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PhotolistViewHolder holder, int position) {
        final Uri imageUri = photolistModels.get(position).getPhotores();
        final InputStream imageStream;
        try {
            imageStream = context.getContentResolver().openInputStream(imageUri);
             final Bitmap selectedImagee = BitmapFactory.decodeStream(imageStream);
           // holder.imageView2.setImageBitmap(selectedImagee);
            holder.imageView.setImageBitmap(selectedImagee);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        holder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photolistModels.remove(position);
                notifyDataSetChanged();
            }
        });
        holder.filePath.setText(photolistModels.get(position).getPhotoname());
    }

    @Override
    public int getItemCount() {
        return photolistModels.size();
    }

    class PhotolistViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        ImageView imageView2;
        TextView filePath;
        public PhotolistViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView12);
            imageView2 = itemView.findViewById(R.id.imageView13);
            filePath = itemView.findViewById(R.id.textView2);
        }
    }
}
