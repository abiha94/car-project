package com.app.carnic.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.carnic.Model.EzharatModel;
import com.app.carnic.Model.PhotolistModel;
import com.app.carnic.R;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class EzharatlistAdapter extends RecyclerView.Adapter<EzharatlistAdapter.PhotolistViewHolder> {

    List<EzharatModel> photolistModels = new ArrayList<>();
    Context context;

    public EzharatlistAdapter(List<EzharatModel> photolistModels, Context context) {
        this.photolistModels = photolistModels;
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public PhotolistViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.recy_ezharat,parent,false);

        return new PhotolistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull PhotolistViewHolder holder, int position) {

        holder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photolistModels.remove(position);
                notifyDataSetChanged();
            }
        });
        String ezhara  =photolistModels.get(position).getCarshenash()+" - " +photolistModels.get(position).getMoshtari();

        holder.filePath.setText(ezhara);
    }

    @Override
    public int getItemCount() {
        return photolistModels.size();
    }

    class PhotolistViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView2;

        TextView filePath;
        public PhotolistViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imageView2 = itemView.findViewById(R.id.imageView13);
            filePath = itemView.findViewById(R.id.textView2);
        }
    }
}
