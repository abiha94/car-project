package com.app.carnic.Model;

import android.net.Uri;

public class PhotolistModel {
    String photoname;
    Uri photores;

    public Uri getPhotores() {
        return photores;
    }

    public void setPhotores(Uri photores) {
        this.photores = photores;
    }

    public String getPhotoname() {
        return photoname;
    }

    public void setPhotoname(String photoname) {
        this.photoname = photoname;
    }

    public PhotolistModel(Uri photores, String photoname) {
        this.photores = photores;
        this.photoname = photoname;
    }
  public PhotolistModel() {

    }
}
