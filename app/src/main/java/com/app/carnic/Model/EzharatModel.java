package com.app.carnic.Model;

public class EzharatModel {
    String carshenash,moshtari;

    public String getCarshenash() {
        return carshenash;
    }

    public void setCarshenash(String carshenash) {
        this.carshenash = carshenash;
    }

    public String getMoshtari() {
        return moshtari;
    }

    public void setMoshtari(String moshtari) {
        this.moshtari = moshtari;
    }

    public EzharatModel(String carshenash, String moshtari) {
        this.carshenash = carshenash;
        this.moshtari = moshtari;
    }
    public EzharatModel() {
        new EzharatModel("","");
    }
}
