package com.app.carnic.Model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;

public class Step3Model {
    List<PhotolistModel> models;
    Drawable drawable;

    public Step3Model() {
        models = new ArrayList<>();
    }

    public List<PhotolistModel> getModels() {
        return models;
    }

    public void setModels(List<PhotolistModel> models) {
        this.models = models;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
