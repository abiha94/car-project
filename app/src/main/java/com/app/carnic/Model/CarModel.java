package com.app.carnic.Model;

public class CarModel {
    String Chassis,OwnerName,PlateLetter;
    int ChassisId,OwnerId,PlateTwo,PlateThree,PlateIR;

    public String getChassis() {
        return Chassis;
    }

    public void setChassis(String chassis) {
        Chassis = chassis;
    }

    public String getOwnerName() {
        return OwnerName;
    }

    public void setOwnerName(String ownerName) {
        OwnerName = ownerName;
    }

    public String getPlateLetter() {
        return PlateLetter;
    }

    public void setPlateLetter(String plateLetter) {
        PlateLetter = plateLetter;
    }

    public int getChassisId() {
        return ChassisId;
    }

    public void setChassisId(int chassisId) {
        ChassisId = chassisId;
    }

    public int getOwnerId() {
        return OwnerId;
    }

    public void setOwnerId(int ownerId) {
        OwnerId = ownerId;
    }

    public int getPlateTwo() {
        return PlateTwo;
    }

    public void setPlateTwo(int plateTwo) {
        PlateTwo = plateTwo;
    }

    public int getPlateThree() {
        return PlateThree;
    }

    public void setPlateThree(int plateThree) {
        PlateThree = plateThree;
    }

    public int getPlateIR() {
        return PlateIR;
    }

    public void setPlateIR(int plateIR) {
        PlateIR = plateIR;
    }
}
